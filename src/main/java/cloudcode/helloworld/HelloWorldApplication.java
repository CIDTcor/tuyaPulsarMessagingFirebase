package cloudcode.helloworld;
import com.alibaba.fastjson.JSON;
import org.json.*;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.FirebaseApp;
import com.google.firebase.database.*;
import java.io.FileInputStream;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import cloudcode.helloworld.mq.*;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ClassPathResource;

import java.io.InputStream;

import com.google.auth.oauth2.GoogleCredentials;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/** This class serves as an entry point for the Spring Boot app. */
@SpringBootApplication
public class HelloWorldApplication {
public static String deviceState = "";
public static JSONArray devStatus;
public static String bizCode = "";
  private static final Logger logger = LoggerFactory.getLogger(HelloWorldApplication.class);

  public static void main(final String[] args) throws Exception {
    String port = System.getenv("PORT");
    if (port == null) {
      port = "8080";
      logger.warn("$PORT environment variable not set, defaulting to 8080");
    }
    SpringApplication app = new SpringApplication(HelloWorldApplication.class);
    app.setDefaultProperties(Collections.singletonMap("server.port", port));

    // Start the Spring Boot application.
    app.run(args);
    logger.info(
        "Hello from Cloud Run! The container started successfully and is listening for HTTP requests on " + port);

    //DATABASE CONFIG
    //NOTA: PARA SUBIR A KUBERNEETES ES NECESARIO
    Resource resource = new ClassPathResource("intelihogardb-firebase-adminsdk-25d8j-e5c9258d6f.json");

    InputStream input = resource.getInputStream();

    FirebaseOptions options = new FirebaseOptions.Builder()
            .setCredentials(GoogleCredentials.fromStream(input))
            .setDatabaseUrl("https://intelihogardb.firebaseio.com")
            .build();
    ///////////////////
    //FileInputStream serviceAccount =
    //        new FileInputStream("src/main/java/cloudcode/helloworld/intelihogardb-firebase-adminsdk-25d8j-e5c9258d6f.json");

    //FirebaseOptions options = new FirebaseOptions.Builder()
    //        .setCredentials(GoogleCredentials.fromStream(serviceAccount))
    //        .setDatabaseUrl("https://intelihogardb.firebaseio.com")
    //        .build();

    FirebaseApp.initializeApp(options);
    ///////////////////

    //SUSCRIBE TO PULSAR DATA
   String url = MqConfigs.US_SERVER_URL;
    String accessId = "7a9hmj7taotw7qite0qe";
    String accessKey = "3fedc14ee40a430d93b0b7c98afc932c";
    MqConsumer mqConsumer = MqConsumer.build().serviceUrl(url).accessId(accessId).accessKey(accessKey)
            .maxRedeliverCount(3).messageListener(message -> {
                System.out.println("message receiver = ");
              String jsonMessage = new String(message.getData());
              MessageVO vo = JSON.parseObject(jsonMessage, MessageVO.class);
              String data = AESBase64Utils.decrypt(vo.getData(), accessKey.substring(8, 24));

              JSONObject dataObject = new JSONObject(data);
                System.out.println("dataObject = "+dataObject);
               if (dataObject.has("status")) {
                    devStatus = new JSONArray(dataObject.get("status").toString());
                } else {
                    devStatus = new JSONArray("[{code:NoStatus,value:0}]");
                }
              JSONObject status = new JSONObject(devStatus.get(0).toString());
              String devId = dataObject.get("devId").toString();
                if (dataObject.has("bizCode")) {
                    bizCode = dataObject.get("bizCode").toString();
                } else {
                    bizCode = "";
                }
                System.out.println("state = "+bizCode);
              switch (bizCode) {
                case "online":
                    deviceState = "Online";
                  break;
                  case "offline":
                      deviceState = "offline";
                      break;
                  default:
                      deviceState = "";
                      break;
              }
              //GET LOCATIONID
              DatabaseReference locationRef = FirebaseDatabase.getInstance().getReference("tuyaDevices");
              locationRef.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                  for (DataSnapshot tuyaDevices: dataSnapshot.getChildren()) {
                    String locationId = tuyaDevices.getKey();
                    String value = tuyaDevices.getValue().toString();
                    if (value.contains(devId)) {
                      DeviceModel device = new DeviceModel();
                      device.setDate();
                        System.out.println("deviceState"+deviceState);
                      if (deviceState.equals("offline")) {
                          device.setStatus(deviceState);
                      } else {
                          device.setStatus(status.get("value").toString());
                      }
                      device.setValue("");
                      setDeviceStatus(devId,locationId,device);
                      System.out.println("Status changed.");
                    }
                  }
                }

                @Override
                public void onCancelled(DatabaseError error) {
                  System.out.println("NO TUYA DEVICES");
                }
              });
            });
    mqConsumer.start();
    /////////////////////
  }

  public static void setDeviceStatus(String deviceId,String locationId,DeviceModel device) {
    DatabaseReference ref = FirebaseDatabase.getInstance().getReference("locations/"+locationId+"/logs/"+deviceId);
    Map<String, Object> deviceUpdates = new HashMap<>();
    deviceUpdates.put("date", device.getDate());
    deviceUpdates.put("status", device.getStatus());
    deviceUpdates.put("value", device.getValue());
    ref.updateChildrenAsync(deviceUpdates);
  }

  public static class DeviceModel {

    String Value,Status;
    Long DeviceDate;

    public DeviceModel(String value, String status, Long deviceDate) {
      Value = value;
      Status = status;
      DeviceDate = deviceDate;
    }

    public DeviceModel() {
    }

    public String getValue() {
      return Value;
    }

    public void setValue(String value) {
      Value = value;
    }


    public String getStatus() {
      if (Status.equals("true")) {
        return "on";
      } else {
          if (Status.equals("offline")) {
              return Status;
          } else {
              return "off";
          }
      }
    }

    public void setStatus(String status) {
      Status = status;
    }


    public Long getDate() {
      return DeviceDate;
    }

    public void setDate() {
      Date date = new Date();
      DeviceDate = date.getTime();
    }
  }
}
